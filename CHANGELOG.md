# Changelog

## 2.1.0

* Fix multiple row bugs in WikiTableScraper
* Replace `<hr />`  with `\n` in WikiTableScraper

## 2.0.1

* Set fallback encoding to utf-8
* Add `_soup` on `HtmlScraper`
* Add `_download_image` in `utils`

## 2.0.0

* Refactor to aliceplex
